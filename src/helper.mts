import { clone, getIbAndGib, IbAndGib } from '@ibgib/ts-gib';
import { Factory_V1 as factory } from '@ibgib/ts-gib/dist/V1/factory.mjs';
import * as h from '@ibgib/ts-gib/dist/helper.mjs';
import { IbGib_V1 } from '@ibgib/ts-gib/dist/V1/types.mjs';

let crypto: any = globalThis.crypto;
let { subtle } = crypto;

export function fooClone<T>(x: T): T {
    return clone(x);
}

export function fooGetIbAndGib(): IbAndGib {
    return getIbAndGib({ ibGib: { ib: 'ib', gib: 'gib' } });
}

export async function fooFactory(): Promise<IbGib_V1> {
    let result = await factory.firstGen({
        ib: 'yo',
        parentIbGib: factory.primitive({ ib: 'yo primitive yo' }),
        dna: true,
        nCounter: true,
        tjp: {
            timestamp: true,
            uuid: true,
        },
    });
    console.dir(result.newIbGib);
    return result.newIbGib;
}

let msg = h.extractErrorMsg("yo");
console.log(msg);