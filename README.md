# 🚧 wip.

I am pulling out/refactoring code from
[ionic-gib](https://github.com/wraiford/ibgib/tree/v0.2.729/ionic-gib) project
into this lib.

# forward

I have created a working MVP for ibgib that shows some of the potential for the
architecture:

* try it out
  * https://ibgib.space
* pre-refactored MVP codebase on GitHub
  * `ionic-gib`: https://github.com/wraiford/ibgib/tree/v0.2.729/ionic-gib
  * "pre-refactored" = doesn't use this core-gib lib, but rather is the
    springboard/"carcass" for it.
* base ibgib protocol graphing substrate lib on GitLab
  * `ts-gib`: https://gitlab.com/ibgib/ts-gib/-/tree/master

# capacitor-gib

The original ionic-gib uses capacitor, so the name "ionic-gib" was already taken.
This is the flip side of that, with ionic/capacitor specific code for creating
applications using ionic capacitor.
